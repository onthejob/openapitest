package com.example.demo;


import com.example.demo.model.Name;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyName {

    @GetMapping("/my-name")
    public Name getMyName() {
        System.out.println("I was called");
        return new Name();
    }
}
