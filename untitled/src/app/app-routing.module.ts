import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MyNameComponent} from "./components/my-name/my-name.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";

const routes: Routes = [
  { path: 'my-name', component: MyNameComponent },
  { path: '**' , component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
