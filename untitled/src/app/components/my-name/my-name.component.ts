import { Component } from '@angular/core';
import {MyNameService} from "../../services/my-name/my-name.service";
import {OnInit} from "@angular/core";

@Component({
  selector: 'app-my-name',
  templateUrl: './my-name.component.html',
  styleUrls: ['./my-name.component.css']
})
export class MyNameComponent implements OnInit{

  public myName: String = "No name";

  constructor(private myNameService: MyNameService) {
  }

  ngOnInit(): void {
    this.myNameService.getMyName().subscribe( {
      next: (name) => {
      console.log("name:" +name.first);
      this.myName = name.first;
    },
      error: (err) => {
        console.log(err)}
    }) ;
  }


  public getMyName(): String {
    return this.myName;
  }

}
