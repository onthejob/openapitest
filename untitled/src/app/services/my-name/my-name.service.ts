import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Name} from "../../interface/name";

@Injectable({
  providedIn: 'root'
})
export class MyNameService {

  constructor(private _httpClient: HttpClient) { }

  public getMyName() {
    console.log("calling")
    return this._httpClient.get<Name>("/api/my-name");
  }
}
